from tkinter import *
import math,random
import argparse
from PIL import ImageTk, Image
import io


parser = argparse.ArgumentParser(description='Forest Fire Simulator 2019')
parser.add_argument('--cell_size', metavar='cell size', type=int, default=60,
                    help='Cell size in pixel')
parser.add_argument('--padding', metavar='padding', type=int, default=0, help='Padding between cell and grid')
parser.add_argument('--rows', metavar='rows', type=int, default=3, help='Grid rows')
parser.add_argument('--cols', metavar='cols', type=int, default=3, help='Grid cols')
parser.add_argument('--speed', metavar='speed', type=float, default=.5, help='Animation speed in sec.')
parser.add_argument('--aforestation', metavar='aforestation', default=.8, type=float, help='Aforestation rate')
parser.add_argument('--image', metavar='image', type=bool, default=False, help='Show image instead of color')
parser.add_argument('--save', metavar='save', type=bool, default=False, help='Save animation as png for each frame')
parser.add_argument('--init', metavar='init', default=None, help='Filepath to ini file to init automa')

args = parser.parse_args()

SHOW_IMAGE = args.image
SAVE = args.save
GRID_OFFSET = args.cell_size
PADDING = args.padding
ROWS = args.rows
COLS = args.cols
ANIMATION_SPEED = int(args.speed * 1000)
FOREST_PERCENTAGE = args.aforestation
INI = args.init

LARGEUR = GRID_OFFSET * ROWS + PADDING
HAUTEUR = GRID_OFFSET * COLS + PADDING
FOREST = 1
FIRE = 2
ASH = 3
EMPTY = 0
EARTH = 4

IMG_SIZE = (GRID_OFFSET - PADDING, GRID_OFFSET - PADDING)

"""
Const configuration, maybe should be external ..
"""
FOREST_COLOR = '#27ae60'
FIRE_COLOR = '#e74c3c'
ASH_COLOR = '#7f8c8d'
EMPTY_COLOR = '#2c3e50'
EARTH_COLOR = '#84817a'

FOREST_IMG = Image.open('tiles/forest.png')
ASH_IMG = Image.open('tiles/ash.png')
TERRAIN_IMG = Image.open('tiles/terrain.png')
EMPTY_IMG = Image.open('tiles/empty.png')
FIRE_IMG = Image.open('tiles/fire.png')

FOREST_IMG = FOREST_IMG.resize(IMG_SIZE, Image.ANTIALIAS)
ASH_IMG = ASH_IMG.resize(IMG_SIZE, Image.ANTIALIAS)
TERRAIN_IMG = TERRAIN_IMG.resize(IMG_SIZE, Image.ANTIALIAS)
EMPTY_IMG = EMPTY_IMG.resize(IMG_SIZE, Image.ANTIALIAS)
FIRE_IMG = FIRE_IMG.resize(IMG_SIZE, Image.ANTIALIAS)

grid = []

"""
Root frame init, have to be init before using ImageTk
"""
root = Tk()
root.title("Forest Fire Simulator 2019")

"""
Defining Texture when using image
"""

FOREST_TEXTURE = ImageTk.PhotoImage(FOREST_IMG)
ASH_TEXTURE = ImageTk.PhotoImage(ASH_IMG)
TERRAIN_TEXTURE = ImageTk.PhotoImage(TERRAIN_IMG)
EMPTY_TEXTURE = ImageTk.PhotoImage(EMPTY_IMG)
FIRE_TEXTURE = ImageTk.PhotoImage(FIRE_IMG)

"""
Canvas creation
"""
canvas = Canvas(root, height=HAUTEUR, width=LARGEUR, bg='white', cursor="plus" )
canvas.grid(row=0, columnspan=2)
canvas.grid = []

"""
Buttons creation
"""
icon = PhotoImage(file='icon.png')
eye_icon = PhotoImage(file='eye.png')
exit_icon = PhotoImage(file='icon_exit.png')
regen_icon = PhotoImage(file='regen.png')
help_icon = PhotoImage(file='help.png')
help3_icon = PhotoImage(file='help3.png')

root.iconphoto(False, icon)

def launch_callback():
    if not button_launch.ANIMATE:
        button_launch.ANIMATE = True
        animation()
        button_launch.config(text='Pause simulation')
    else:
        button_launch.ANIMATE = False
        button_launch.config(text='Restart simulation')


button_launch = Button(root, text='Launch simulation', command=launch_callback, compound="left", image=eye_icon)
button_launch.grid(row=1, column=1)
button_launch.ANIMATE = False

button_exit = Button(root, text='Quit', command=root.quit, compound="left", image=exit_icon)
button_exit.grid(row=2, column=1)

label_help = Label(root, text=' <Left click>: on automa to set cell on fire', compound='left', image=help_icon)
label_help.grid(row=1, column=0, sticky='w')
label_help = Label(root, text=' <Right click>: on automa to set cell to forest', compound='left', image=help3_icon)
label_help.grid(row=2, column=0, sticky='w')

"""
Menu init
"""

def regenerate_automa():
    global grid
    grid = generate_grid()
    draw_grid(grid)
    if button_launch.ANIMATE:
        launch_callback()

menu = Menu(root)
file_menu = Menu(menu, tearoff=0)
file_menu.add_command(label='Regenerate automa', command=regenerate_automa, image=regen_icon, compound='left')
file_menu.add_separator()
file_menu.add_command(label='Quit', command=root.quit, image=exit_icon, compound='left')
menu.add_cascade(label='File', menu=file_menu, image=icon, compound='left')
root.config(menu=menu)



root.rowconfigure(0, weight=1)
root.columnconfigure(0, weight=1)

def draw_rectangle(row, col, color='blue', rectangle=None):
    x1 = PADDING + (col * GRID_OFFSET)
    y1 = PADDING + (row * GRID_OFFSET)
    x2 = GRID_OFFSET + (col * GRID_OFFSET)
    y2 = GRID_OFFSET + (row * GRID_OFFSET)
    if not SHOW_IMAGE:
        if isinstance(color, int):
            if color == EMPTY:
                color = EMPTY_COLOR
            elif color == FOREST:
                color = FOREST_COLOR
            elif color == FIRE:
                color = FIRE_COLOR
            elif color == ASH:
                color = ASH_COLOR
            else:
                color = EARTH_COLOR
        if rectangle == None:
            return canvas.create_rectangle(x1, y1, x2, y2, fill=color, outline="")
        else:
            canvas.itemconfig(rectangle, fill=color)
    else:
        if color == EMPTY:
            texture = EMPTY_TEXTURE
        elif color == FOREST:
            texture = FOREST_TEXTURE
        elif color == FIRE:
            texture = FIRE_TEXTURE
        elif color == ASH:
            texture = ASH_TEXTURE
        else:
            texture = TERRAIN_TEXTURE
        if rectangle is None:
            return canvas.create_image(x1, y1, image=texture, anchor=NW)
        else:
            canvas.itemconfig(rectangle, image=texture)

x = 1
while(x <= HAUTEUR):
    canvas.create_line(0, x, LARGEUR, x, fill="#bdc3c7")
    x += GRID_OFFSET

y = 1
while (y <= LARGEUR):
    canvas.create_line(y, 0, y, HAUTEUR, fill='#bdc3c7')
    y += GRID_OFFSET







def clic_callback(event):
    """ Gestion de l'événement clic_callback gauche sur la zone graphique """
    # position du pointeur de la souris
    X = event.x
    Y = event.y
    cell_value = None
    
    if event.type == EventType.ButtonPress:
        if event.num == 1:
            cell_value = FIRE
        elif event.num == 3:
            cell_value = FOREST
    elif event.type == EventType.Motion:
        if event.state == 256:
            cell_value = FIRE
        elif event.state == 1024:
            cell_value = FOREST
    
    if cell_value == FIRE:
        canvas.config(cursor='plus')
    elif cell_value == FOREST:
        canvas.config(cursor='spraycan')

    if cell_value:
        col = int(event.x / GRID_OFFSET)
        row = int(event.y / GRID_OFFSET)
        grid[row][col] = cell_value
    draw_grid(grid)
    

canvas.bind('<Button-1>', clic_callback)
canvas.bind('<B1-Motion>', clic_callback)
canvas.bind('<Button-3>', clic_callback)
canvas.bind('<B3-Motion>', clic_callback)


def generate_from_ini():
    with open(INI) as ini_file:
        grid_ini = ini_file.readlines()
        grid = []
        canvas.grid = []
        for row_index, line in enumerate(grid_ini):
            canvas.grid.append([])
            grid.append([])
            for value in line:
                if value == "1":
                    grid[row_index].append(FOREST)
                else:
                    grid[row_index].append(EARTH)
                canvas.grid[row_index].append(None)
        return grid


def generate_grid():
    if INI:
        return generate_from_ini()
    else:
        rand = random.Random()
        grid = []
        canvas.grid = []
        for row in range(0, int(HAUTEUR / GRID_OFFSET)):
            canvas.grid.append([])
            grid.append([])
            for col in range(0, int(LARGEUR / GRID_OFFSET)):
                if rand.random() < FOREST_PERCENTAGE:
                    grid[row].append(FOREST)
                else:
                    grid[row].append(EARTH)
                canvas.grid[row].append(None)
        return grid

def draw_grid(grid):
    for row_index, row in enumerate(grid):
        for col_index, col in enumerate(row):
            rectangle = canvas.grid[row_index][col_index]
            rectangle = draw_rectangle(row_index, col_index, col, rectangle)
            if canvas.grid[row_index][col_index] is None:
                canvas.grid[row_index][col_index] = rectangle
    if SAVE:
        save_canvas()

def get_cell_value(grid, row, col):
    if row == -1 or col == -1:
        return None
    try:
        return grid[row][col]
    except IndexError:
        return None

def copy_grid(grid):
    new_grid = []
    for row_index, row in enumerate(grid):
        new_grid.append([])
        for col_index, col in enumerate(row):
            new_grid[row_index].append(col)
    return new_grid

def update_grid():
    new_grid = copy_grid(grid)
    for row_index, row in enumerate(grid):
        for col_index, col in enumerate(row):
            if col == FOREST:
                if get_cell_value(grid, row_index, col_index - 1) == FIRE:
                    new_grid[row_index][col_index] = FIRE
                elif get_cell_value(grid, row_index, col_index + 1) == FIRE:
                    new_grid[row_index][col_index] = FIRE
                elif get_cell_value(grid, row_index - 1, col_index) == FIRE:
                    new_grid[row_index][col_index] = FIRE
                elif get_cell_value(grid, row_index + 1, col_index) == FIRE:
                    new_grid[row_index][col_index] = FIRE
            elif col == FIRE:
                new_grid[row_index][col_index] = ASH
            elif col == ASH:
                new_grid[row_index][col_index] = EMPTY

    for row_index, row in enumerate(new_grid):
        for col_index, col in enumerate(row):
            grid[row_index][col_index] = new_grid[row_index][col_index]

def save_canvas():
    import datetime
    ps = canvas.postscript(colormode='color')
    img = Image.open(io.BytesIO(ps.encode('utf-8')))
    img.save('screens/%s.jpg' % datetime.datetime.now().strftime('%d-%m-%Y-%H-%M-%S'), 'jpeg')

def animation():
    if button_launch.ANIMATE:
        update_grid()
        draw_grid(grid)
        root.after(ANIMATION_SPEED, animation)


grid = generate_grid()
draw_grid(grid)
root.mainloop()
